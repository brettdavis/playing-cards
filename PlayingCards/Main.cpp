//Brett Davis
//Playing Cards

#include <iostream>
#include <conio.h>

using namespace std;

enum Suit
{
	Hearts, Diamonds, Clubs, Spades
};

enum Rank
{
	Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace
};

struct Card
{
	Suit suit;
	Rank rank;
};

int main()
{
	_getch();
	return 0;
}